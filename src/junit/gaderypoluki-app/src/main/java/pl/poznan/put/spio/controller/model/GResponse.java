package pl.poznan.put.spio.controller.model;

public class GResponse {

	private String origin;
	private String translated;

	public String getOrigin() {
		return origin;
	}

	public String getTranslated() {
		return translated;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public void setTranslated(String translated) {
		this.translated = translated;
	}

}

package pl.poznan.put.spio;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.poznan.put.spio.translator.GaDeRyPoLuKi;

public class GaDeRyPoLuKiTest {

	/**
	 * Uporządkowanie środowiska testowego JUnit. Metoda uruchamiana tylko raz,
	 * dla całej klasy testującej.
	 */
	@AfterClass
	public static void cleanUpClass() {
		// not this time
	}

	/**
	 * Przygotowanie środowiska testowego JUnit. Metoda uruchamiana tylko raz,
	 * dla całej klasy testującej.
	 */
	@BeforeClass
	public static void initClass() {
		// not this time
	}

	private GaDeRyPoLuKi g;

	/**
	 * Uporządkowanie środowska testowego JUnit. Metoda uruchamiana po każdym
	 * przypadku testowym @Test.
	 */
	@After
	public void cleanUp() {
		g = null;// odrobinę nadmiarowe zwolnienie zasobów
	}

	/**
	 * Przygotowanie środowiska testowego JUnit. Metoda uruchamiana przed każdym
	 * przypadkiem testowym @Test.
	 */
	@Before
	public void init() {
		g = new GaDeRyPoLuKi();// utworzenie obiektu translatora
	}

	/**
	 * Test translacji, gdy litery nie są przewidziane w mapie translacji.
	 */
	@Test
	public void shouldStayNotTranslated() {
		// given
		final String msg = "LOK";

		// when
		final String result = g.translate(msg);// translacja, wynik zapisany w
		// 'result'

		// then
		Assert.fail("do uzupełnienia");// fail rzuca błąd bezwarunkowy
	}

	/**
	 * Podany w teście argument o wartości null spowoduje rzucenie wyjątku
	 * {@link NullPointerException}, który w tym miejscu jest oczekiwany.
	 */
	@Test(expected = NullPointerException.class)
	public void shouldThrowExceptionWhenTranslateNull() {
		// given

		// when
		g.translate(null);// tylko translacja, powinna rzucić wyjątek

		// then
	}

	/**
	 * Przykładowy test sprawdzania translacji napisu.
	 */
	@Test
	public void shouldTranslate() {
		// given
		final String msg = "lok";

		// when
		final String result = g.translate(msg);// translacja, wynik zapisany w
		// 'result'

		// then
		Assert.assertEquals("upi", result);// metoda sprawdzająca
	}

	/**
	 * Test translacji, ignorowana wielkość liter.
	 */
	@Test
	public void shouldTranslateIgnoreCase() {
		// given
		final String msg = "KOT";

		// when
		final String result = g.translateIgnoreCase(msg);

		// then
		Assert.fail("do uzupełnienia");
	}

	/**
	 * Sprawdzenie d�ugo�ci kodu do translacji.
	 */
	@Test
	public void testCodeLength() {
		// given

		// when
		int size = g.getCodeLength();

		// then
		Assert.fail("do uzupełnienia");
	}

	/**
	 * Sprawdzenie czy dany znak będzie podlegał translacji.
	 */
	@Test
	public void testIsTranslated1() {
		// given
		final String c = "Z";

		// when
		final boolean result = g.isTranslatable(c);

		// then
		Assert.assertFalse(result);
	}

	/**
	 * Sprawdzenie czy dany znak będzie podlegał translacji.
	 */
	@Test
	public void testIsTranslated2() {
		// given
		final String c = "g";

		// when
		final boolean result = g.isTranslatable(c);

		// then
		Assert.fail("do uzupełnienia");
	}

	/**
	 * Test translacji, kolejne litery do sprawdzenia.
	 */
	@Test
	public void testTranslate3() {
		// given

		// when
		final String result = g.translate("????");// translacja, wynik zapisany
		// w 'result'

		// then
		Assert.fail("do uzupełnienia");// fail rzuca błąd bezwarunkowy
	}
}
